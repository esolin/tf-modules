variable "name" {
  type = string
}

variable "external" {
  type    = bool
  default = false
}
