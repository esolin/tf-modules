output "id" {
  value = openstack_networking_network_v2.network.id
}

output "tags" {
  value = openstack_networking_network_v2.network.all_tags
}

output "mtu" {
  value = openstack_networking_network_v2.network.mtu
}
