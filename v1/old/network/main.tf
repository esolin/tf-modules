terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

resource "openstack_networking_network_v2" "network" {
  name           = var.name
  admin_state_up = true
  external       = var.external
}
