variable "name" {
  type = string
}

variable "image_name" {
  type = string
}

variable "flavor_name" {
  type = string
}

variable "availability_zone" {
  type    = string
  default = "nova"
}

variable "key_pair" {
  type = string
}

variable "user_data" {
  type    = string
  default = ""
}

# variable "port_id" {
#   type = string
# }

variable "network_id" {
  type = string
}

variable "security_groups" {
  default = []
}

variable "metadata" {
  # type = string
  default = {}
}


