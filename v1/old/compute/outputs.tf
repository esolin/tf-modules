output "id" {
  value = openstack_compute_instance_v2.vm.id
}

output "name" {
  value = openstack_compute_instance_v2.vm.name
}

output "private_ip" {
  value = openstack_compute_instance_v2.vm.access_ip_v4
}

output "floating_ip" {
  value = openstack_compute_instance_v2.vm.floating_ip
}

# output "port_id" {
#   value = openstack_compute_instance_v2.vm.network.port_id
# }

output "network" {
  value = openstack_compute_instance_v2.vm.network
}
