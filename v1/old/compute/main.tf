terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

resource "openstack_compute_instance_v2" "vm" {
  name              = var.name
  image_name        = var.image_name
  flavor_name       = var.flavor_name
  availability_zone = var.availability_zone
  key_pair          = var.key_pair
  user_data         = var.user_data

  network {
    uuid = var.network_id
  }

  security_groups = var.security_groups

  metadata = var.metadata
}
