terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

resource "openstack_networking_router_v2" "router" {
  name                = var.name
  admin_state_up      = true
  external_network_id = var.external_network_id
}
