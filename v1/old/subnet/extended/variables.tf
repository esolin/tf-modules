variable "name" {
  type = string
}

variable "network_id" {
  type = string
}

variable "cidr" {
  type = string
}

variable "gateway_ip" {
  type = string
}

variable "enable_dhcp" {
  type    = bool
  default = true
}

variable "dns_nameservers" {
  type    = list(string)
  default = []
}

variable "allocation_pool" {
  type = object({
    start = string
    end   = string
  })
}
