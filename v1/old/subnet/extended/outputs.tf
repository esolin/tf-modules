
output "id" {
  value = openstack_networking_subnet_v2.subnet.id
}

output "cidr" {
  value = openstack_networking_subnet_v2.subnet.cidr
}
