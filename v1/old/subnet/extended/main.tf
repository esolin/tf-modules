terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

resource "openstack_networking_subnet_v2" "subnet" {
  name            = var.name
  network_id      = var.network_id
  cidr            = var.cidr
  ip_version      = 4
  gateway_ip      = var.gateway_ip
  enable_dhcp     = var.enable_dhcp
  dns_nameservers = var.dns_nameservers


  allocation_pool {
    start = var.allocation_pool.start
    end   = var.allocation_pool.end
  }
}
