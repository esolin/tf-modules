variable "name" {
  type = string
}

variable "network_id" {
  type = string
}

variable "cidr" {
  type = string
}
