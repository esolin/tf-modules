output "id" {
  value = openstack_compute_instance_v2.vm.id
}

output "name" {
  value = openstack_compute_instance_v2.vm.name
}

output "private_ip" {
  value = openstack_compute_instance_v2.vm.access_ip_v4
}

output "floating_ip" {
  value = one(openstack_networking_floatingip_v2.floating_ip[*].address)
}

output "network" {
  value = openstack_compute_instance_v2.vm.network
}
