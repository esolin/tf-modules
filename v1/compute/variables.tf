variable "name" {
  type = string
}

variable "image_name" {
  type = string
}

variable "flavor_name" {
  type = string
}

variable "availability_zone" {
  type    = string
  default = "nova"
}

variable "key_pair" {
  type = string
}

variable "user_data" {
  type    = string
  default = ""
}

variable "network_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "security_groups" {
  default = []
}

variable "public" {
  type    = bool
  default = false
}

variable "metadata" {
  # type = string
  default = {}
}


