terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

resource "openstack_compute_instance_v2" "vm" {
  name              = var.name
  image_name        = var.image_name
  flavor_name       = var.flavor_name
  availability_zone = var.availability_zone
  key_pair          = var.key_pair
  user_data         = var.user_data

  network {
    port = openstack_networking_port_v2.port.id
  }

  metadata = var.metadata
}

# Create port which is used to connect instance to network.
resource "openstack_networking_port_v2" "port" {
  name           = "port_${var.name}"
  network_id     = var.network_id
  admin_state_up = true

  security_group_ids = var.security_groups

  fixed_ip {
    subnet_id = var.subnet_id
  }
}

# Create floating ip IF var.public is set to true. Otherwise not.
resource "openstack_networking_floatingip_v2" "floating_ip" {
  count = var.public ? 1 : 0

  pool = "external_network"
}

resource "openstack_networking_floatingip_associate_v2" "floating_ip_associate" {
  count = var.public ? 1 : 0

  floating_ip = openstack_networking_floatingip_v2.floating_ip[count.index].address
  port_id     = openstack_networking_port_v2.port.id
}
