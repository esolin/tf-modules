variable "name" {
  type = string
}

variable "network" {
  type = object({
    external       = optional(bool)
    admin_state_up = optional(bool)
    shared         = optional(bool)

    segments = optional(object({
      physical_network = optional(string)
      segmentation_id  = optional(number)
      network_type     = optional(string)
    }))
  })

  # If problems arise, delete the default. If the default value is not present, 
  # the network variable will be needed in the modules even if it is empty.
  # such as:
  # network = {}
  default = {}
}

variable "subnet" {
  type = object({
    cidr            = string
    enable_dhcp     = optional(bool)
    dns_nameservers = optional(list(string))
    gateway_ip      = optional(string)

    allocation_pool = optional(object({
      start = string
      end   = string
    }))
  })
}
