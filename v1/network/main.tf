terraform {
  experiments = [module_variable_optional_attrs]
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

resource "openstack_networking_network_v2" "network" {
  name           = var.name
  admin_state_up = var.network.admin_state_up
  external       = var.network.external

  dynamic "segments" {
    for_each = var.network.segments != null ? [1] : []

    content {
      physical_network = var.network.segments.physical_network
      segmentation_id  = var.network.segments.segmentation_id
      network_type     = var.network.segments.network_type
    }
  }
}

resource "openstack_networking_subnet_v2" "subnet" {
  # count = length(subnets)

  name            = "${var.name}_subnet"
  network_id      = openstack_networking_network_v2.network.id
  cidr            = var.subnet.cidr
  ip_version      = 4
  gateway_ip      = var.subnet.gateway_ip
  enable_dhcp     = var.subnet.enable_dhcp
  dns_nameservers = var.subnet.dns_nameservers


  dynamic "allocation_pool" {
    for_each = var.subnet.allocation_pool != null ? [1] : []
    content {
      start = var.subnet.allocation_pool.start
      end   = var.subnet.allocation_pool.end
    }
  }
}

