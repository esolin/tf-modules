
output "name" {
  value = openstack_identity_project_v3.project.name
}

output "member" {
  value = openstack_identity_role_assignment_v3.member.user_id
}

