data "openstack_identity_user_v3" "admin" {
  name = "admin"
}

data "openstack_identity_role_v3" "admin" {
  name = "admin"
}

data "openstack_identity_role_v3" "member" {
  name = "member"
}
