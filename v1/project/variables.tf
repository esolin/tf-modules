
variable "name" {
  type = string
}

variable "user_id" {
  type    = string
  default = null
}

variable "compute_quotas" {
  type = object({
    keypairs     = optional(number)
    ram          = optional(number)
    cores        = optional(number)
    floating_ips = optional(number)
    instances    = optional(number)
  })
  default = {}
}

variable "default_quota" {
  type    = number
  default = 3
}
