resource "openstack_identity_project_v3" "project" {
  name = var.name
}

resource "openstack_identity_role_assignment_v3" "member" {
  user_id    = var.user_id
  project_id = openstack_identity_project_v3.project.id
  role_id    = data.openstack_identity_role_v3.member.id
}

resource "openstack_compute_quotaset_v2" "compute_quotas" {
  project_id   = openstack_identity_project_v3.project.id
  key_pairs    = var.compute_quotas.keypairs != null ? var.compute_quotas.keypairs : var.default_quota
  ram          = var.compute_quotas.ram != null ? var.compute_quotas.ram * 1024 : var.default_quota * 1024
  cores        = var.compute_quotas.cores != null ? var.compute_quotas.cores : var.default_quota
  floating_ips = var.compute_quotas.floating_ips != null ? var.compute_quotas.floating_ips : var.default_quota
  instances    = var.compute_quotas.instances != null ? var.compute_quotas.instances : var.default_quota
}

# add admin to all projects
resource "openstack_identity_role_assignment_v3" "admin" {
  user_id    = data.openstack_identity_user_v3.admin.id
  project_id = openstack_identity_project_v3.project.id
  role_id    = data.openstack_identity_role_v3.admin.id
}
