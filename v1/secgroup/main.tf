terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

resource "openstack_networking_secgroup_v2" "secgroup" {
  name        = var.name
  description = "Security group for ${var.name}"
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule" {
  description       = "Security group rule for ${var.name}"
  direction         = var.direction
  ethertype         = var.ethertype
  protocol          = var.protocol
  port_range_min    = var.port_range_min
  port_range_max    = var.port_range_max
  remote_ip_prefix  = var.remote_ip_prefix
  security_group_id = openstack_networking_secgroup_v2.secgroup.id
}
