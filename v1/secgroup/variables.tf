
variable "name" {
  type = string
}

variable "direction" {
  type = string
}

variable "ethertype" {
  type = string
}

variable "protocol" {
  type    = string
  default = "tcp"
}

variable "port_range_min" {
  type = number
}

variable "port_range_max" {
  type = number
}

variable "remote_ip_prefix" {
  type = string
}

