variable "name" {
  type = string
}

variable "external_network_id" {
  type = string
}

variable "subnet_id" {
  type = string
}
